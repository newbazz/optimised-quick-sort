### Graph : Time Complexities of Sorting Algorithms
<img src="images/comparison.png"/>

### Comparison with other sorting algorithms

|Algorithm Sort 	|Algorithm Average |	Time Best |	Time Worst |	Features Space |	Features Stability|
|-----------------------|------------------|--------------|---------------|--------------------|------------------------|
|Quicksort 	|O(n*log(n)) |	O(n*log(n)) |	O(n2) |	Constant |	Stable|
|Bubble sort 	|O(n2) 	|O(n2) |	O(n2) |	Constant |	Stable|
|Modified Bubble sort| 	O(n2) |	O(n) 	|O(n2) 	|Constant |	Stable|
|Selection Sort |	O(n2) |	O(n2) |	O(n2) 	|Constant |	Stable|
|Insertion Sort |	O(n2) |	O(n) 	|O(n2) |	Constant |	Stable|
|Heap Sort 	|O(n*log(n)) |	O(n*log(n)) |	O(n*log(n)) |	Constant |	Unstable|
|Merge Sort 	|O(n*log(n)) |	O(n*log(n)) |	O(n*log(n)) |	Depends |	Stable|
